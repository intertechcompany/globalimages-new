<?php namespace Intertech\Globalsite\Components;

use Cms\Classes\ComponentBase;
use Intertech\Globalsite\Models\Advantage as HomeAdvantage;

class Advantage extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Advantage Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $advantages = HomeAdvantage::where('is_enabled', true)
                                   ->orderBy('sort_order', 'asc')
                                   ->get();

        $this->page['advantages'] = $advantages;
    }

}