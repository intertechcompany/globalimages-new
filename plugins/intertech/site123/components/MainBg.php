<?php namespace Intertech\Globalsite\Components;

use Cms\Classes\ComponentBase;
use Intertech\Globalsite\Models\Page;
use Intertech\Globalsite\Models\HidePage;

class MainBg extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'MainBg Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'page' => [
                'type' => 'string',
                'label' => 'page'
            ],
            'slug' => [
                'type' => 'string',
                'label' => 'slug'
            ],
            'foto' => [
                'type' => 'string',
                'label' => 'foto'
            ]
        ];
    }

    public function onRun()
    {
        $page = $this->property('page');
        $slug = $this->property('slug');
        $foto = $this->property('foto');
        if ($page == 'text') {
            $return = Page::where('slug', $slug)->first();

            /**
             * Get Hide page for show page
             */
            $DL = HidePage::where('slug', $slug)->where('is_enabled', true)->first();

            if (!empty($return) && sizeof($return) > 0) {
                $main = $return;
            } else {
                if (!empty($DL) && sizeof($DL) > 0) {
                    $main = $DL;
                } else {
                    $main = Page::where('cms_page', $page)->first();
                }
            }
        } else {
            $main = Page::where('cms_page', $page)->first();
        }

        if (!empty($main->main_title)) {
            $this->page->title = $main->main_title;
        }

        $this->page['classpage'] = $this->generateClassNameHtml($page);
        $this->page['main'] = $main;
    }

    public function generateClassNameHtml($page)
    {
        if (!isset($page)) return false;

        if ($page == 'photo') $return = 'foto';
        elseif ($page == 'music') $return = 'music';
        elseif ($page == 'subscription') $return = 'foto';
        elseif ($page == 'services') $return = 'foto';
        elseif ($page == 'video') $return = 'video';
        elseif ($page == 'text') $return = 'foto';
        elseif ($page == 'contact') $return = 'contacts';
        elseif ($page == 'team') $return = 'contacts';

        return $return;

    }

}