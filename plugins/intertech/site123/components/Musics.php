<?php namespace Intertech\Globalsite\Components;

use Cms\Classes\ComponentBase;
use Intertech\Globalsite\Models\Music;

class Musics extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Musics Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $musics = Music::where('is_enabled', true)
                       ->orderBy('sort_order', 'asc')
                       ->get();

        $this->page['musics'] = $musics;
    }

}