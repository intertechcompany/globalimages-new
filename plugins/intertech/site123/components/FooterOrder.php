<?php namespace Intertech\Globalsite\Components;

use Cms\Classes\ComponentBase;
use Intertech\Globalsite\Models\HidePage;

class FooterOrder extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'FooterOrder Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'type' => 'string',
                'label' => 'slug'
            ]
        ];
    }

    public function onRender()
    {
        $slug = $this->property('slug');
        $DL = HidePage::where('slug', $slug)->where('is_enabled', true)->first();

        if (isset($DL->is_feedback)) {
            $prop = $DL->is_feedback;
        } else {
            $prop = true;
        }

        $this->page['show'] = $prop;
    }

}