<?php namespace Intertech\Globalsite\Components;

use Cms\Classes\ComponentBase;
use Intertech\Globalsite\Models\ErrorSettings;

class ErrorPage extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'ErrorPage Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $error = ErrorSettings::instance();

        $this->page['error'] = $error;
    }

}