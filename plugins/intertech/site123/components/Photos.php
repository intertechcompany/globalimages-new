<?php namespace Intertech\Globalsite\Components;

use Cms\Classes\ComponentBase;
use Intertech\Globalsite\Models\Photo;

class Photos extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Photos Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $photos = Photo::where('is_enabled', true)
                       ->orderBy('sort_order', 'asc')
                       ->get();

        $this->page['photos'] = $photos;
    }

}