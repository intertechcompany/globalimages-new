<?php namespace Intertech\Globalsite\Components;

use Cms\Classes\ComponentBase;
use Intertech\Globalsite\Models\Page;

class Pages extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Pages Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $pages = Page::where('is_enabled', true)
                        ->orderBy('sort_order', 'asc')
                        ->where('is_main', true)
                        ->get()
                        ->toArray();

        $this->page['pages'] = $pages;
    }

}