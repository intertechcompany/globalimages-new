<?php namespace Intertech\Globalsite\Components;

use Cms\Classes\ComponentBase;
use Intertech\Globalsite\Models\Slider;

class Sliders extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Sliders Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $sliders = Slider::where('is_enabled', true)
                         ->orderBy('sort_order', 'asc')
                        ->get();

        $this->page['sliders'] = $sliders;
    }

}