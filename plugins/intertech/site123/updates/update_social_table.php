<?php namespace Feegleweb\Octoshop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdateSocialTable extends Migration
{

    public function up() {
        Schema::table('intertech_globalsite_socials', function($table) {
            $table->string('type')->unique();
        });
    }

    public function down() {
        Schema::table('intertech_globalsite_socials', function($table) {
            $table->dropColumn('type');
        });
    }

}