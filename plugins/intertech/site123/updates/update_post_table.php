<?php namespace Feegleweb\Octoshop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdatePostTable extends Migration
{

    public function up() {
        Schema::table('intertech_globalsite_posts', function($table) {
            $table->text('link')->nullable();
        });
    }

    public function down() {
        Schema::table('intertech_globalsite_posts', function($table) {
            $table->dropColumn('link');
        });
    }

}
