<?php namespace Feegleweb\Octoshop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdateSubscriptionTable extends Migration
{

    public function up() {
        Schema::table('intertech_globalsite_subscriptions', function($table) {
            $table->string('is_image')->change();
        });
    }

    public function down() {
        Schema::table('intertech_globalsite_subscriptions', function($table) {
            $table->dropColumn('is_image');
        });
    }

}
