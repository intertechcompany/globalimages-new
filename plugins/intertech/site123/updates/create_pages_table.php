<?php namespace Intertech\Globalsite\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePagesTable extends Migration
{
    public function up()
    {
        Schema::create('intertech_globalsite_pages', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->string('title_main')->nullable();
            $table->string('title_description')->nullable();
            $table->text('description')->nullable();
            $table->string('type')->nullable();
            $table->text('link')->nullable();
            $table->boolean('is_main')->default(true);
            $table->boolean('is_footer')->default(false);
            $table->boolean('is_enabled')->default(true);
            $table->integer('sort_order')->unsigned()->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('intertech_globalsite_pages');
    }
}
