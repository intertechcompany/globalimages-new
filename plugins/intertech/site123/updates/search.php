<?php namespace Feegleweb\Octoshop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class Seach extends Migration
{

    public function up() {
        Schema::table('intertech_globalsite_services', function($table) {
            $table->string('search')->nullable();
        });
    }

    public function down() {
        Schema::table('intertech_globalsite_services', function($table) {
            $table->dropColumn('search');
        });
    }

}
