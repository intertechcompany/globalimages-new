<?php namespace Feegleweb\Octoshop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdatePageTableSlug extends Migration
{

    public function up() {
        Schema::table('intertech_globalsite_pages', function($table) {
            $table->string('slug')->index()->unique();
        });
    }

    public function down() {
        Schema::table('intertech_globalsite_pages', function($table) {
            $table->dropColumn('slug');
        });
    }

}
