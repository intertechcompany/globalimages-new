<?php namespace Intertech\Globalsite\Models;

use Model;
use October\Rain\Database\Traits\Validation;

/**
 * InformationSettings Model
 */
class InformationSettings extends Model
{

    use Validation;

    public $implement = [
        'System.Behaviors.SettingsModel',
        'RainLab.Translate.Behaviors.TranslatableModel'
    ];

    public $translatable = [
        'footer'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $settingsCode = 'intertech_globalsite_information_settings';

    public $settingsFields = 'fields.yaml';

    public $rules = [
        'link' => [
            'url',
            'between:1,255'
        ]
    ];

}