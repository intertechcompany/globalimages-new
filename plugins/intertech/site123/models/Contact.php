<?php namespace Intertech\Globalsite\Models;

use Model;
use October\Rain\Database\Traits\Validation;
use October\Rain\Database\Traits\Sortable;

/**
 * Contact Model
 */
class Contact extends Model
{

    use Validation, Sortable;

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'title',
        'address'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'intertech_globalsite_contacts';

    public $rules = [
        'title' => 'required',
        'email' => 'email',
        // 'second_email' => 'email',
        'address' => 'between:1,255',
        'image' => 'required'
    ];

    public $attributeNames = [
        'title' => 'Название',
        'address' => 'Адрес',
        'email' => 'E-mail',
        'second_email' => 'E-mail под названием',
        'image' => 'Изображение'
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'title',
        'address',
        'email',
        'second_email'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'image' => 'System\Models\File'
    ];
    public $attachMany = [];

}