<?php namespace Intertech\Globalsite\Models;

use Model;
use October\Rain\Database\Traits\Validation;

/**
 * Ask Model
 */
class Ask extends Model
{

    use Validation;

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'ask',
        'name',
        'message'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'intertech_globalsite_asks';

    public $rules = [
        'ask' => 'required',
        'name' => 'required',
        'message' => 'required',
        'email' => [
            'email',
            'required',
            'between:1,255'
        ],
        'phone' => 'required'
    ];

    public $attributeNames = [
        'ask' => 'Вопрос',
        'name' => 'Имя',
        'message' => 'Сообщение',
        'email' => 'E-mail',
        'phone' => 'Телефон'
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'ask',
        'name',
        'email',
        'phone',
        'message'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}