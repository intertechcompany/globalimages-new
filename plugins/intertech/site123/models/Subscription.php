<?php namespace Intertech\Globalsite\Models;

use Model;
use October\Rain\Database\Traits\Validation;
use October\Rain\Database\Traits\Sortable;

/**
 * Subscription Model
 */
class Subscription extends Model
{

    use Validation, Sortable;

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'title',
        'description'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'intertech_globalsite_subscriptions';

    public $rules = [
        'title' => 'required',
        'description' => 'required'
    ];

    public $attributeNames = [
        'title' => 'Название',
        'description' => 'Описание'
    ];
    
    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'title',
        'description'
    ];

    /**
     * @var array Relations
     */
    public $attachOne = [
        'image' => ['System\Models\File', 'public' => true]
    ];

    public function beforeValidate()
    {
        $model = $this->input;
        // echo "<pre>"; print_r($model);
    }
}