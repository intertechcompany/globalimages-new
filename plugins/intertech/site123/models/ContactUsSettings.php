<?php namespace Intertech\Globalsite\Models;

use Model;

/**
 * ContactUsSettings Model
 */
class ContactUsSettings extends Model
{

    public $implement = [
        'System.Behaviors.SettingsModel',
        'RainLab.Translate.Behaviors.TranslatableModel'
    ];

    public $translatable = [
        'about_address'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $settingsCode = 'intertech_globalsite_contact_us_settings';

    public $settingsFields = 'fields.yaml';

}