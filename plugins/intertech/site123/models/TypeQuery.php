<?php namespace Intertech\Globalsite\Models;

use Model;
use October\Rain\Database\Traits\Validation;
use October\Rain\Database\Traits\Sortable;

/**
 * TypeQuery Model
 */
class TypeQuery extends Model
{

    use Validation, Sortable;

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'title'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'intertech_globalsite_type_queries';

    /**
     * @var bool
     */
    public $timestamps = false;
    
    public $rules = [
        'title' => [
            'required',
            'between:1,255'
        ]
    ];

    public $attributeNames = [
        'title' => 'Название'
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

}