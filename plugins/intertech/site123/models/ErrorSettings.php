<?php namespace Intertech\Globalsite\Models;

use Model;

/**
 * ErrorSettings Model
 */
class ErrorSettings extends Model
{

    public $implement = [
        'System.Behaviors.SettingsModel',
        'RainLab.Translate.Behaviors.TranslatableModel'
    ];

    public $translatable = [
        'description'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $settingsCode = 'intertech_globalsite_error_settings';

    public $settingsFields = 'fields.yaml';

}