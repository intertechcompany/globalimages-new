<?php namespace Intertech\Globalsite\Models;

use Model;
use October\Rain\Database\Traits\Validation;
use October\Rain\Database\Traits\Sortable;

/**
 * Sliders Model
 */
class Slider extends Model
{

    use Validation, Sortable;

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'title',
        'description'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'intertech_globalsite_sliders';

    /**
     * @var bool
     */
    public $timestamps = false;

    public $rules = [
        'description' => 'required',
        'is_main' => 'required',
        'link' => 'url'
    ];

    public $attributeNames = [
        'title' => 'Название',
        'description' => 'Описание',
        'image' => 'Изображение',
        'is_main' => 'Главное изображение',
        'link' => 'Ссылка'
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'title',
        'description'
    ];

    /**
     * @var array Relations
     */
    public $attachOne = [
        'image' => 'System\Models\File',
        'video' => 'System\Models\File'
    ];

    public function filterFields($fields, $context = null)
    {
        if ($fields->is_video->value && $fields->is_image->value) {
            $fields->video->hidden = true;
            $fields->image->hidden = true;
        } elseif (!$fields->is_video->value && !$fields->is_image->value) {
            $fields->video->hidden = true;
            $fields->image->hidden = true;
        } elseif ($fields->is_video->value) {
            $fields->image->hidden = true;
            $fields->is_image->value = false;
        } elseif ($fields->is_image->value) {
            $fields->video->hidden = true;
            $fields->is_video->value = false;
        }
    }

}