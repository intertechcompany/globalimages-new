<?php namespace Intertech\Globalsite\Models;

use Model;
use Intertech\Globalsite\Traits\PageTrait;
use October\Rain\Database\Traits\Sortable;
use Intertech\Globalsite\Traits\ParameterOptions;
use October\Rain\Database\Traits\Validation;
use October\Rain\Database\Traits\SimpleTree;

/**
 * Page Model
 */
class Page extends Model
{

    use Validation, Sortable, ParameterOptions, PageTrait, SimpleTree;

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'title',
        'title_main',
        'title_description',
        'description',
        'main_title'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'intertech_globalsite_pages';

    /**
     * @var bool
     */
    public $timestamps = false;


    public $rules = [
        'title' => [
            'required',
            'between:1,255'
        ],
        'title_description' => 'between:1,255',
        'link' => [
            'url',
            'between:1,255'
        ],
        'slug' => [
            'between:1,255',
            'unique:intertech_globalsite_pages'
        ],
        'main_title' => 'required'
    ];

    public $attributeNames = [
        'title' => 'Название страницы',
        'title_main' => 'Главное название',
        'title_description' => 'Название',
        'description' => 'Описание',
        'link' => 'Ссылка',
        'image' => 'Изображение',
        'slug' => 'Slug (ссылка для страницы)',
        'main_title' => 'Title Название'
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'title',
        'title_main',
        'title_description',
        'description',
        'link',
        'image'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'image' => 'System\Models\File',
        'video' => 'System\Models\File'
    ];
    public $attachMany = [];

    public function filterFields($fields, $context = null)
    {
        if ($fields->is_video->value && $fields->is_image->value) {
            $fields->video->hidden = true;
            $fields->image->hidden = true;
        } elseif (!$fields->is_video->value && !$fields->is_image->value) {
            $fields->video->hidden = true;
            $fields->image->hidden = true;
        } elseif ($fields->is_video->value) {
            $fields->image->hidden = true;
            $fields->is_image->value = false;
        } elseif ($fields->is_image->value) {
            $fields->video->hidden = true;
            $fields->is_video->value = false;
        }
    }

}