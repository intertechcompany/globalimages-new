<?php namespace Intertech\Globalsite\Models;

use Model;
use October\Rain\Database\Traits\Validation;

/**
 * HidePage Model
 */
class HidePage extends Model
{

    use Validation;

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'title',
        'title_main',
        'description',
        'main_title'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'intertech_globalsite_hide_pages';

    /**
     * @var bool
     */
    public $timestamps = false;


    public $rules = [
        'title' => [
            'required',
            'between:1,255'
        ],
        'slug' => [
            'between:1,255',
            'unique:intertech_globalsite_pages'
        ],
        'main_title' => 'required'
    ];

    public $attributeNames = [
        'title' => 'Название страницы',
        'title_main' => 'Главное название',
        'description' => 'Описание',
        'image' => 'Изображение',
        'slug' => 'Slug (ссылка для страницы)',
        'main_title' => 'Title Название'
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $attachOne = [
        'image' => 'System\Models\File',
        'video' => 'System\Models\File'
    ];

    public function filterFields($fields, $context = null)
    {
        if ($fields->is_video->value && $fields->is_image->value) {
            $fields->video->hidden = true;
            $fields->image->hidden = true;
        } elseif ($fields->is_video->value) {
            $fields->image->hidden = true;
            $fields->is_image->value = false;
        } elseif ($fields->is_image->value) {
            $fields->video->hidden = true;
            $fields->is_video->value = false;
        }
    }

}