<?php namespace Intertech\Globalsite\Components;

use Cms\Classes\ComponentBase;
use Intertech\Globalsite\Models\Galery;
use Intertech\Lakestudio\Traits\ComponentsTrait;

class ContactForm extends ComponentBase
{
    use ComponentsTrait;

    public function componentDetails()
    {
        return [
            'name'        => 'Galeries Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $galeries = Galery::where('is_enabled', true)
                          ->orderBy('sort_order', 'asc')
                          ->get();

        $this->page['galeries'] = $galeries;
    }

}