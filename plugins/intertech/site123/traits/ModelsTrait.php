<?php

namespace Intertech\Seo\Traits;

trait ModelsTrait
{
    /**
     * @param $fields
     * @param null $context
     */
    public function filterFields($fields, $context = null)
    {
        $change_field = post('fields')[0];

        switch ($change_field) {
            case 'cms_page':
                $this->processCmsPageField($fields);
                break;

            case 'path':
                $this->processPathField($fields);
                break;
        }
    }

    /**
     * @param $fields
     */
    private function processCmsPageField($fields)
    {
        if ($fields->type->value == 'cms_page') {
            $fields->cms_page->hidden = false;
        }
    }

    /**
     * @param $fields
     */
    private function processPathField($fields)
    {
        switch ($fields->type->value) {
            case 'cms_page':
                if (!is_null($fields->cms_page->value) && !empty($fields->cms_page->value)) {
                    $page = $this->findCmsPageByFilename($fields->cms_page->value);
                    $fields->path->value = $page->url;

                    return;
                }
                break;

            case 'url':
                break;
        }

        $fields->path->value = '';
    }
}
