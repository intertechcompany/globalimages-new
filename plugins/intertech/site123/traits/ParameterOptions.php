<?php

namespace Intertech\Globalsite\Traits;

use Cms\Classes\Page;

trait ParameterOptions
{
    /**
     * @var array
     */
    private $types = [
        0 => 'Select type',
        'cms_page' => 'CMS page',
        'url' => 'URL',
    ];

    /**
     * @return array
     */
    public function getTypeOptions()
    {
        return $this->types;
    }
}