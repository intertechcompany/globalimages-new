<?php namespace Intertech\Globalsite;

use Route;
use Session;

Route::post('ajax-form', function () {
    // return post('hash');
    $return = Session::put('music-block', substr(post('hash'), 1));
    return $return;
});