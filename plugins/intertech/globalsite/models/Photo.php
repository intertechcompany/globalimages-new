<?php namespace Intertech\Globalsite\Models;

use Model;
use October\Rain\Database\Traits\Validation;
use October\Rain\Database\Traits\Sortable;

/**
 * Photo Model
 */
class Photo extends Model
{
    use Validation, Sortable;

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'title',
        'description'
    ];

    protected $jsonable = ['categories'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'intertech_globalsite_photos';

    public $rules = [
        'title' => 'required',
        'description' => 'required|max:285',
        'image' => 'required',
        'link' => [
            'url',
            'required',
            'between:1,255'
        ],
        'categories' => 'required'
    ];

    public $attributeNames = [
        'title' => 'Название',
        'description' => 'Описание',
        'image' => 'Изображение',
        'link' => 'Ссылка'
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'title',
        'description',
        'categories'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'image' => 'System\Models\File'
    ];
    public $attachMany = [];

    public function beforeValidate()
    {
        // $array = [];
        // if (sizeof($this->categories) > 3) {
        //     Flash::error('Количество категорий больше 3');

        //     throw new ValidationException(['username' => 'Sorry that username is already taken!']);
        // }

        // if (sizeof($this->categories) >= 1) {
        //     foreach ($this->categories as $item) {
        //         if (sizeof($item['subcategories']) > 4) {
        //             Flash::error('Количество подкатегорий больше 4');

        //             throw new ValidationException(['username' => 'Sorry that username is already taken!']);
        //         }
        //     }
        // }
    }

}