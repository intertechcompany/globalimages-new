<?php namespace Intertech\Globalsite\Models;

use Model;
use October\Rain\Database\Traits\Validation;
use October\Rain\Database\Traits\Sortable;

/**
 * Service Model
 */
class Service extends Model
{

    use Validation, Sortable;

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'title',
        'description'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'intertech_globalsite_services';

    public $rules = [
        'title' => [
            'required',
            'between:1,255'
        ],
        'description' => 'required',
        'link' => [
            'required',
            'url',
            'between:1,255'
        ],
        'image' => 'required'
    ];

    public $attributeNames = [
        'title' => 'Название',
        'description' => 'Описание',
        'link' => 'Ссылка',
        'image' => 'Изображение'
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'title',
        'description',
        'link',
        'image'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'image' => 'System\Models\File'
    ];
    public $attachMany = [];

}