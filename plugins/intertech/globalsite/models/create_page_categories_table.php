<?php namespace Intertech\Globalsite\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePageCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('intertech_globalsite_page_categories', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('type')->nullable();
            $table->text('link')->nullable();
            $table->integer('page_id')->unsigned()->nullable();
            $table->foreign('page_id')
                ->references('id')
                ->on('intertech_globalsite_pages')
                ->onDelete('set null');
            $table->boolean('is_enabled')->default(true);
            $table->integer('sort_order')->unsigned()->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('intertech_globalsite_page_categories');
    }
}
