<?php namespace Intertech\Globalsite\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateGettyimagesTable extends Migration
{
    public function up()
    {
        Schema::create('intertech_globalsite_gettyimages', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->text('link');
            $table->boolean('is_image')->default(false);
            $table->boolean('is_video')->default(false);
            $table->longText('categories')->nullable();
            $table->boolean('is_enabled')->default(true);
            $table->integer('sort_order')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('intertech_globalsite_gettyimages');
    }
}
