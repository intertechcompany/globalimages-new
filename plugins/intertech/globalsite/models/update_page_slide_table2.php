<?php namespace Feegleweb\Octoshop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdatePageSlideTable2 extends Migration
{

    public function up() {
        Schema::table('intertech_globalsite_page_sliders', function($table) {
            $table->integer('page_id')->unsigned();
            $table->foreign('page_id')
                ->references('id')
                ->on('intertech_globalsite_pages')
                ->onDelete('cascade');
        });
    }

    public function down() {
        Schema::table('intertech_globalsite_page_sliders', function($table) {
            $table->dropForeign('intertech_globalsite_page_sliders_page_id_foreign');
            $table->dropColumn('page_id');
        });
    }

}
