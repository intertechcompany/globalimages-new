<?php namespace Intertech\Globalsite\Models;

use Model;
use October\Rain\Database\Traits\Validation;

/**
 * utm Model
 */
class Utm extends Model
{
    use Validation;

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'title'
    ];
    /**
     * @var string The database table used by the model.
     */
    public $table = 'intertech_globalsite_utms';

    /**
     * @var bool
     */
    public $timestamps = false;

    public $rules = [
        'title' => 'required',
        'utm' => 'required',
        'email' => 'email'
    ];

    public $attributeNames = [
        'title' => 'Название формы',
        'description' => 'UTM идентификатоп',
        'email' => 'E-mail',
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];


}