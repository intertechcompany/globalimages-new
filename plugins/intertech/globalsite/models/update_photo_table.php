<?php namespace Feegleweb\Octoshop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdateVideoTable2 extends Migration
{

    public function up() {
        Schema::table('intertech_globalsite_photos', function(Blueprint $table) {
            $table->longText('categories')->nullable();
        });
    }

    public function down() {
        Schema::table('intertech_globalsite_photos', function(Blueprint $table) {
            $table->dropColumn('categories');
        });
    }

}
