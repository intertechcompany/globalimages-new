<?php namespace Intertech\Globalsite\Models;

use Model;
use October\Rain\Database\Traits\Validation;
use October\Rain\Database\Traits\Sortable;

/**
 * Post Model
 */
class Post extends Model
{

    use Validation, Sortable;

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'title'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'intertech_globalsite_posts';

    public $rules = [
        'title' => 'required',
        'image' => 'required',
        'link' => [
            'url',
            'required',
            'between:1,255'
        ]
    ];

    public $attributeNames = [
        'title' => 'Название',
        'image' => 'Изображение',
        'link' => 'Ссылка'
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'title',
        'image'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'image' => 'System\Models\File'
    ];
    public $attachMany = [];

}