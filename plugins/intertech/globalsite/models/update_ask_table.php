<?php namespace Feegleweb\Octoshop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdateAskTable extends Migration
{

    public function up() {
        Schema::table('intertech_globalsite_asks', function($table) {
            $table->string('utm_title')->nullable();
            $table->string('utm_mark')->nullable();
        });
    }

    public function down() {
        Schema::table('intertech_globalsite_asks', function($table) {
            $table->dropColumn('utm_title');
            $table->dropColumn('utm_mark');
        });
    }

}
