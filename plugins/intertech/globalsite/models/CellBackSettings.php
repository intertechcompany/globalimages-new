<?php namespace Intertech\Globalsite\Models;

use Model;

/**
 * ContactUsSettings Model
 */
class CellBackSettings extends Model
{

    public $implement = [
        'System.Behaviors.SettingsModel',
        'RainLab.Translate.Behaviors.TranslatableModel'
    ];

    public $translatable = [
        'about_address',
        'image'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $settingsCode = 'intertech_globalsite_cell_back_settings';

    public $settingsFields = 'fields.yaml';

    public $attachOne = [
        'image' => 'System\Models\File'
    ];

}