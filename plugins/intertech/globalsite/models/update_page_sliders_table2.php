<?php namespace Intertech\Globalsite\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdatePageSlidersTable2 extends Migration
{
    public function up()
    {
        Schema::table('intertech_globalsite_page_sliders', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('position')->nullable();
        });
    }

    public function down()
    {
        Schema::table('intertech_globalsite_page_sliders', function($table) {
            $table->dropColumn('position');
        });
    }
}
