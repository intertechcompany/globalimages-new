<?php namespace Intertech\Globalsite\Models;

use Model;
use October\Rain\Database\Traits\Validation;

/**
 * ContactSettings Model
 */
class ContactSettings extends Model
{

    use Validation;

    public $implement = [
        'System.Behaviors.SettingsModel',
        'RainLab.Translate.Behaviors.TranslatableModel'
    ];

    public $translatable = [
        'description',
        'address',
        'title',
        'address',
        'cu_title',
        'cu_company',
        'cu_description'
        // 'phone_description'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $settingsCode = 'intertech_globalsite_contact_settings';

    public $rules = [
        'email' => 'email',
        'cu_email' => 'email'
    ];

    public $attributeNames = [
        'email' => 'E-mail',
        'cu_email' => 'E-mail'
    ];

    public $settingsFields = 'fields.yaml';

}