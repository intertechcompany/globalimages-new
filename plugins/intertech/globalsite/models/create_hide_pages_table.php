<?php namespace Intertech\Globalsite\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateHidePagesTable extends Migration
{
    public function up()
    {
        Schema::create('intertech_globalsite_hide_pages', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->string('main_title');
            $table->string('title_main');
            $table->longText('description')->nullable();
            $table->string('slug')->index()->unique();
            $table->boolean('is_enabled')->default(true);
            $table->string('is_image')->nullable();
            $table->string('is_video')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('intertech_globalsite_hide_pages');
    }
}
