<?php namespace Feegleweb\Octoshop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdateSliderTable extends Migration
{

    public function up() {
        Schema::table('intertech_globalsite_sliders', function($table) {
            $table->string('is_main');
        });
    }

    public function down() {
        Schema::table('intertech_globalsite_sliders', function($table) {
            $table->dropColumn('is_main');
        });
    }

}
