<?php namespace Intertech\Globalsite\Models;

use Model;
use October\Rain\Database\Traits\Sortable;
use October\Rain\Database\Traits\Validation;

use Intertech\Globalsite\Models\Page;

/**
 * PageSlider Model
 */
class PageSlider extends Model
{
    use Validation, Sortable;

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'name',
        'description'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'intertech_globalsite_page_sliders';

    /**
     * @var bool
     */
    public $timestamps = false;
    
    public $rules = [
        'name' => 'required',
        'link' => 'url'
    ];

    public $attributeNames = [
        'name' => 'Название',
        'description' => 'Описание',
        'image' => 'Изображение',
        'link' => 'Ссылка'
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'name',
        'description',
        'page_id',
        'position'
    ];
    
    /**
     * @var array Relations
     */
    public $attachOne = [
        'image' => 'System\Models\File',
        'video' => 'System\Models\File'
    ];

    public function filterFields($fields, $context = null)
    {
        if ($fields->is_video->value && $fields->is_photo->value) {
            $fields->video->hidden = true;
            $fields->image->hidden = true;
        } elseif (!$fields->is_video->value && !$fields->is_photo->value) {
            $fields->video->hidden = true;
            $fields->image->hidden = true;
        } elseif ($fields->is_video->value) {
            $fields->image->hidden = true;
            $fields->is_photo->value = false;
        } elseif ($fields->is_photo->value) {
            $fields->video->hidden = true;
            $fields->is_video->value = false;
        }
    }

    public function scopeFilterReorer($query)
    {
        
    }

}