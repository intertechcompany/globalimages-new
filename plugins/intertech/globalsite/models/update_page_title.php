<?php namespace Feegleweb\Octoshop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdatePageTitleTable extends Migration
{

    public function up() {
        Schema::table('intertech_globalsite_pages', function($table) {
            $table->string('main_title')->nullable();
            $table->string('is_video')->nullable();
        });
    }

    public function down() {
        Schema::table('intertech_globalsite_pages', function($table) {
            $table->dropColumn('main_title');
            $table->dropColumn('is_video');
        });
    }

}
