<?php namespace Feegleweb\Octoshop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdateVideoTable extends Migration
{

    public function up() {
        Schema::table('intertech_globalsite_videos', function($table) {
            $table->boolean('is_image')->default(false);
            $table->boolean('is_video')->default(false);
        });
    }

    public function down() {
        Schema::table('intertech_globalsite_videos', function($table) {
            $table->dropColumn('is_image');
            $table->dropColumn('is_video');
        });
    }

}
