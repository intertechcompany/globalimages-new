<?php namespace Intertech\Globalsite\Components;

use Cms\Classes\ComponentBase;
use Intertech\Globalsite\Models\Post;

class Posts extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Posts Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $posts = Post::where('is_enabled', true)
                     ->orderBy('sort_order', 'asc')
                     ->get();

                     echo "<pre>"; print_r($posts); die();

        $this->page['posts'] = $posts;
    }

}