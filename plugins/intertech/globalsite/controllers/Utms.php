<?php namespace Intertech\Globalsite\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Utms Back-end Controller
 */
class Utms extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Intertech.Globalsite', 'forms', 'utms');
    }
}