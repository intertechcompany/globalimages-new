<?php namespace Intertech\Globalsite\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Hide Pages Back-end Controller
 */
class HidePages extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Intertech.Globalsite', 'globalsite', 'hidepages');
    }
}