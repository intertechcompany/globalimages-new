<?php namespace Intertech\Globalsite\Middlewares;

use Closure;
use Request;
use Session;
use Intertech\Globalsite\Models\Utm;

class DetermineLocation
{
    /**
     * @param $request
     * @param Closure $next
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function handle($request, Closure $next)
    {

        $requestUTM = Request::all();
        $modelUTM = $this->getUTM($requestUTM);

        /**
         * Get utm link and put on session
         */
        $utm_link = $this->generateUTMMark($requestUTM);
        if ($utm_link) {
            $this->putSession('utm_link', $utm_link);
            if ($modelUTM) {
                $this->putSession('utm_id', $modelUTM->id);
            }
        }

        $utmSessinon = $this->getSessionByKey('utm_link');

        // echo "<pre>"; print_r($utmSessinon); die();

        return $next($request);
    }

    public function getUTM($request)
    {
        if (!empty($request)) {

            $getParm = http_build_query($request);
            
            $utm = Utm::where('utm', 'like', '%' . $getParm . '%')->first();

            if (!$utm) {
                return false;
            }

            return $utm;
        }
    }

    public function generateUTMMark($request)
    {

        $getParm = http_build_query($request);

        if ($getParm) {
            return $getParm;
        }

        return false;
    }

    public function putSession($key, $value)
    {
        Session::put($key, $value);
    }

    public function getSessionByKey($key)
    {
        if ($key) {
            $keyUTM = Session::get($key);

            if ($keyUTM) {
                return '?' . $keyUTM;
            }
        }

        return false;
    }
}