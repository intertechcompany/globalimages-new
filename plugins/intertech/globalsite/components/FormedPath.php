<?php namespace Intertech\Globalsite\Components;

use Session;
use Cms\Classes\ComponentBase;
use Intertech\Globalsite\Models\Page;
use Intertech\Globalsite\Models\Video;
use Intertech\Globalsite\Models\Photo;
use Intertech\Globalsite\Models\Music;
use Intertech\Globalsite\Models\HidePage;

class FormedPath extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'MainBg Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
        ];
    }

    public function onRun()
    {
        
    }

    public function onFormedPath()
    {
        $pageId = post('page_id');
        $blockId = post('block_id');
        $categoryId = post('category_id');
        $subCategoryId = post('subcategory_id');
        $cmsPage = post('cms_page');

        $page = $this->formedPagePath($cmsPage);
        $block = $this->formedBlockPath($blockId, $cmsPage);
        $category = $this->formedCategoryPath($categoryId, $cmsPage, $blockId);
        $subCategory = $this->formedSubCategoryPath($subCategoryId, $cmsPage, $categoryId, $blockId);

        if (!$page) {
            $stops = '';
        } else {
            $stops = $page . ' - ' . $block . ' - ' . $category . ' - ' . $subCategory;
        }

        if (post('return')) {
            // $pathStops = $stops;
            // echo "<pre>"; print_r($pathStops);
        }

        $this->page['pathstops'] = $stops;
    }

    public function formedPagePath($cmsPage)
    {
        $page = Page::where('cms_page', $cmsPage)->first();

        $namePage = '';
        if ($page) {
            $namePage = $page->title;
            $nameId = $page->id;

            Session::put('page_title', $namePage);
            Session::put('page_id', $nameId);
        } else {
            $page = Page::where('cms_page', 'photo')->first();

            $namePage = $page->title;
            $nameId = $page->id;

            Session::put('page_title', $namePage);
            Session::put('page_id', $nameId);
        }

        return $namePage;
    }

    public function formedBlockPath($blockId, $cmsPage)
    {
        $page = Page::where('cms_page', $cmsPage)->first();

        if (!$page) {
            return false;
        }

        if ($page->cms_page == 'photo') {
            $photo = Photo::find($blockId);

            if ($photo) {
                Session::put('block_name', 'photo');
                Session::put('block_id', $photo->id);

                $categories = $photo->categories;

                if (sizeof($categories) <= 0) {
                    return false;
                }

                $categoryFirst = array_shift($categories);

                if (!empty($categoryFirst) || isset($categoryFirst['name'])) {
                    Session::put('category', $categoryFirst['name']);
                }

                return $photo->title;
            }
        } elseif ($page->cms_page == 'video') {
            $video = Video::find($blockId);

            if ($video) {
                Session::put('block_name', 'video');
                Session::put('block_id', $video->id);

                $categories = $video->categories;
                if (sizeof($categories) <= 0) {
                    return false;
                }
                $categoryFirst = array_shift($categories);

                if (!empty($categoryFirst) || isset($categoryFirst['name'])) {
                    Session::put('category', $categoryFirst['name']);
                }

                return $video->title;
            }
        } elseif ($page->cms_page == 'music') {
            $music = Music::find($blockId);

            if ($music) {
                Session::put('block_name', 'music');
                Session::put('block_id', $music->id);

                $categories = $music->categories;
                if (sizeof($categories) <= 0) {
                    return false;
                }
                $categoryFirst = array_shift($categories);

                if (!empty($categoryFirst) || isset($categoryFirst['name'])) {
                    Session::put('category', $categoryFirst['name']);
                }

                return $music->title;
            }
        } else {
            return false;
        }
    }

    public function formedCategoryPath($categoryId, $cmsPage, $blockId)
    {
        $page = Page::where('cms_page', $cmsPage)->first();

        if (!$page) {
            return false;
        }

        if ($page->cms_page == 'photo') {
            $photo = Photo::find($blockId);

            if (empty($categoryId)) {
                $categoryId = 1;
            }

            if ($photo) {

                $categories = $photo->categories;

                if (sizeof($categories) > 0) {
                    if (isset($categories[$categoryId])) {
                        return $categories[$categoryId]['name'];
                    }
                }

            }

            return false;
        } elseif ($page->cms_page == 'video') {
            $video = Video::find($blockId);

            if (empty($categoryId)) {
                $categoryId = 1;
            }

            if ($video) {

                $categories = $video->categories;

                if (sizeof($categories) > 0) {
                    if (isset($categories[$categoryId])) {
                        return $categories[$categoryId]['name'];
                    }
                }

            }

            return false;
        } elseif ($page->cms_page == 'music') {
            $music = Music::find($blockId);

            if (empty($categoryId)) {
                $categoryId = 1;
            }

            if ($music) {

                $categories = $music->categories;

                if (sizeof($categories) > 0) {
                    if (isset($categories[$categoryId])) {
                        return $categories[$categoryId]['name'];
                    }
                }

            }

            return false;
        } else {
            return false;
        }
    }

    public function formedSubCategoryPath($subCategoryId, $cmsPage, $categoryId, $blockId)
    {
        $page = Page::where('cms_page', $cmsPage)->first();

        if (!$page) {
            return false;
        }

        if ($page->cms_page == 'photo') {
            $photo = Photo::find($blockId);

            if (empty($categoryId)) {
                $categoryId = 1;
            }

            if (empty($subCategoryId)) {
                $subCategoryId = 1;
            }

            if ($photo) {

                $categories = $photo->categories;

                if (sizeof($categories) > 0) {
                    if (isset($categories[$categoryId])) {

                        $subCategory = $categories[$categoryId];

                        if (isset($subCategory['subcategories'][$subCategoryId]['name'])) {
                            return $subCategory['subcategories'][$subCategoryId]['name'];
                        }

                    }
                }

                return false;
            }
        } elseif ($page->cms_page == 'video') {
            $video = Video::find($blockId);

            if (empty($categoryId)) {
                $categoryId = 1;
            }

            if (empty($subCategoryId)) {
                $subCategoryId = 1;
            }

            if ($video) {

                $categories = $video->categories;

                if (sizeof($categories) > 0) {
                    if (isset($categories[$categoryId])) {

                        $subCategories = $categories[$categoryId];

                        // echo "<pre>"; print_r($subCategories);

                        if (isset($subCategories['subcategories'][$subCategoryId]['name'])) {
                            return $subCategories['subcategories'][$subCategoryId]['name'];
                        }

                    }
                }

                return false;
            }
        } elseif ($page->cms_page == 'music') {
            $music = Music::find($blockId);

            if (empty($categoryId)) {
                $categoryId = 1;
            }

            if (empty($subCategoryId)) {
                $subCategoryId = 1;
            }

            if ($music) {

                $categories = $music->categories;

                if (sizeof($categories) > 0) {
                    if (isset($categories[$categoryId])) {

                        $subCategory = $categories[$categoryId];

                        if (isset($subCategory['subcategories'][$subCategoryId]['name'])) {
                            return $subCategory['subcategories'][$subCategoryId]['name'];
                        }

                    }
                }
                return false;
            } else {
                return false;
            }
        }
    }
}