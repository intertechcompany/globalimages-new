<?php namespace Intertech\Globalsite\Components;

use Cms\Classes\ComponentBase;
use Intertech\Globalsite\Models\Galery;

class Galeries extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Galeries Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $galeries = Galery::where('is_enabled', true)
                          ->orderBy('sort_order', 'asc')
                          ->get();

        $this->page['galeries'] = $galeries;
        // $this->page['show'] = count($galeries);
    }

}