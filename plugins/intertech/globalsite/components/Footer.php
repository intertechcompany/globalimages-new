<?php namespace Intertech\Globalsite\Components;

use Cms\Classes\ComponentBase;
use Intertech\Globalsite\Models\Page;
use Intertech\Globalsite\Models\Social;
use Intertech\Globalsite\Models\InformationSettings;

class Footer extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Footer Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $leftFooterPages = Page::where('is_enabled', true)
             ->where('is_footer', true)
             ->orderBy('sort_order', 'asc')
             ->where('position', 'left')
             ->get()
         ;
         $rightFooterPages = Page::where('is_enabled', true)
             ->where('is_footer', true)
             ->orderBy('sort_order', 'asc')
             ->where('position', 'right')
             ->get()
         ;
        $socials = Social::where('is_enabled', true)
             ->where('is_footer', true)
             ->orderBy('sort_order', 'asc')
             ->get()
         ;

        $footer = InformationSettings::instance();

        $this->page['leftFooterPages'] = $leftFooterPages;
        $this->page['rightFooterPages'] = $rightFooterPages;
        $this->page['socials'] = $socials;
        $this->page['footer'] = $footer;
    }

}
