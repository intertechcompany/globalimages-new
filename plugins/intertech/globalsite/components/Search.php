<?php namespace Intertech\Globalsite\Components;

use Request;
use Redirect;
use Cms\Classes\ComponentBase;
use Intertech\Globalsite\Models\Music;
use Intertech\Globalsite\Models\Photo;
use Intertech\Globalsite\Models\Video;
use Intertech\Globalsite\Models\Gettyimage;
use Intertech\Globalsite\Traits\ComponentsTrait;

class Search extends ComponentBase
{

    use ComponentsTrait;

    public $url = 'http://www.gettyimages.com/photos/';
    public $second_url = '?family=creative&license=rm&phrase=';
    public $last_url = '&excludenudity=true&sort=best#license';

    public $music_url = 'http://www.gettyimages.com/Music/Search?msm.SearchTerms=';
    public $music_second_url = '';
    public $music_last_url = '&msm.SearchCollectionFilter=2';

    public $video_url = 'http://www.gettyimages.com/videos/';
    public $video_second_url = '?phrase=';
    public $video_last_url = '&excludenudity=true&sort=best#license';

    public $pages = [
        'music',
        'video',
        'photo',
        'gettyimages',
    ];

    public function componentDetails()
    {
        return [
            'name'        => 'Search Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'partial' => [
                'type' => 'string',
                'label' => 'slug'
            ],
            'slug' => [
                'type' => 'string',
                'label' => 'slug'
            ]
        ];
    }

    public function onRun()
    {
        $slug = $this->property('slug');

        $musics = Music::where('is_enabled', true)->get();
        $photos = Photo::where('is_enabled', true)->get();
        $videos = Video::where('is_enabled', true)->get();

        $this->page['musics'] = $musics;
        $this->page['photos'] = $photos;
        $this->page['videos'] = $videos;
        $this->page['disabled'] = $this->property('disabled');

        if ($slug == 'all') {
            $this->page['realpage'] = 'photo';
        }
    }

    public function getData()
    {
        $slug = $this->property('slug');

        $musics = Music::where('is_enabled', true)->get();
        $photos = Photo::where('is_enabled', true)->get();
        $videos = Video::where('is_enabled', true)->get();
        $gettyimages = Gettyimage::where('is_enabled', true)->get();

        $url = Request::url();
        // $exp = explode('/', $url);
        foreach ($this->pages as $page) {
            if (in_array($page, [$slug])) {
                $this->page['realpage'] = $page;
            }
        }

        $this->page['musics'] = $musics;
        $this->page['photos'] = $photos;
        $this->page['videos'] = $videos;
        $this->page['gettyimages'] = $gettyimages;

        return [
            // 'musics' => $musics,
            // 'photos' => $photos,
            // 'videos' => $videos
        ];
    }

    public function onSearch()
    {
        $words = post('word');
        $page = $this->property('page');

        if (!empty($words)) {
            $new_words = explode(' ', $words);

            if (sizeof($new_words) == 1) {
                $formedMoreWords = $words;
            }

            if (sizeof($new_words) > 1) {
                $formedMoreWords = $this->moreWords($new_words);
            }

            if ($page == 'music') {
                return Redirect::to($this->music_url . $formedMoreWords . $this->music_last_url);
            }

            if ($page == 'photo') {
                return Redirect::to($this->url . $formedMoreWords . $this->second_url . $words . $this->last_url);
            }

            if ($page == 'video') {
                return Redirect::to($this->video_url . $formedMoreWords . $this->video_second_url . $words . $this->video_last_url);
            }
        }
    }

    public function moreWords(array $new_words)
    {
        $string = '';

        for ($i = 1; $i <= sizeof($new_words); $i++) {
            if ($i < sizeof($new_words)) {
                $string .= $new_words[$i - 1] . '-';
            }
            $string .= $new_words[$i - 1];
        }
        
        return $string;
    }

    public function onShowForm()
    {
        $blockId = post('block_id');
        Session::put('photo-block', $blockId);
    }

}