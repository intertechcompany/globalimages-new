<?php namespace Intertech\Globalsite\Components;

use Request;
use Session;
use Redirect;
use Cms\Classes\ComponentBase;
use Intertech\Globalsite\Models\Music;

class Musics extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Musics Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        if (!empty(Request::get('hash'))) {
            Session::put('music-block', Request::get('hash'));
        }

        $musics = Music::where('is_enabled', true)
                       ->orderBy('sort_order', 'asc')
                       ->get();

        $this->page['musics'] = $musics;
        $MusicId = Session::get('music-block');
        $musicSession = Music::find($MusicId);
        if ($musicSession) {
            $this->page['block'] = $musicSession;
        }
    }

    public function onShowForm()
    {
        $blockId = post('block_id');
        Session::put('music-block', $blockId);
    }

}