<?php namespace Intertech\Globalsite\Components;

use Cms\Classes\ComponentBase;
use Intertech\Globalsite\Models\Page;
use Intertech\Globalsite\Models\Subscription;
use Intertech\Globalsite\Models\InformationSettings;

class Subscriptions extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Subscriptions Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $subs = Subscription::where('is_enabled', true)
                            ->orderBy('sort_order', 'asc')
                            ->get()->toArray();

        $page = Page::where('cms_page', 'subscription')
                    ->first();

        $link = InformationSettings::instance();

        $this->page['subs'] = $subs;
        $this->page['page'] = $page;
        $this->page['link'] = $link;
    }

}