<?php namespace Intertech\Globalsite\Components;

use Session;
use Request;
use Cms\Classes\ComponentBase;
use Intertech\Globalsite\Models\Photo;

class Photos extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Photos Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        if (!empty(Request::get('hash'))) {
            Session::put('photo-block', Request::get('hash'));
        }

        $photos = Photo::where('is_enabled', true)
                       ->orderBy('sort_order', 'asc')
                       ->get();

        $this->page['photos'] = $photos;

        $photoId = Session::get('photo-block');
        $photoSession = Photo::find($photoId);
        if ($photoSession) {
            $this->page['block'] = $photoSession;
        }


    }

    public function onShowForm()
    {
        $blockId = post('block_id');
        Session::put('photo-block', $blockId);
    }

}