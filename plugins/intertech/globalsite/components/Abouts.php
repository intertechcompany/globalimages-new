<?php namespace Intertech\Globalsite\Components;

use Cms\Classes\ComponentBase;
use Intertech\Globalsite\Models\About;

class Abouts extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Abouts Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $abouts = About::where('is_enabled', true)
                        ->orderBy('sort_order', 'asc')
                        ->get();

        $this->page['abouts'] = $abouts;
    }

}