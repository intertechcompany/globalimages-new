<?php namespace Intertech\Globalsite\Components;

use Cms\Classes\ComponentBase;
use Intertech\Globalsite\Models\ContactSettings;

class ContactDescription extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'ContactDescription Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $description = ContactSettings::instance();
        $this->page['description'] = $description;
    }

}