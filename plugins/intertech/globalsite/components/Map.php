<?php namespace Intertech\Globalsite\Components;

use Cms\Classes\ComponentBase;
use Intertech\Globalsite\Models\ContactSettings;

class Map extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Map Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $map = ContactSettings::instance();
        $this->page['map'] = $map;
    }

}