<?php namespace Intertech\Globalsite\Components;

use Cms\Classes\ComponentBase;
use Intertech\Globalsite\Models\Service;

class Services extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Services Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $services = Service::where('is_enabled', true)
                           ->orderBy('sort_order', 'asc')
                           ->get();

       $this->page['services'] = $services;
    }

}