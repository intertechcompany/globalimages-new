<?php namespace Intertech\Globalsite\Components;

use Session;
use Request;
use Cms\Classes\ComponentBase;
use Intertech\Globalsite\Models\Video;

class Videos extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Videos Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        if (!empty(Request::get('hash'))) {
            Session::put('video-block', Request::get('hash'));
        }

        $videos = Video::where('is_enabled', true)
                       ->orderBy('sort_order', 'asc')
                       ->get();

        $this->page['videos'] = $videos;

        $videoId = Session::get('video-block');
        $videoSession = Video::find($videoId);
        if ($videoSession) {
            $this->page['block'] = $videoSession;
        }
    }

    public function onShowForm()
    {
        $blockId = post('block_id');
        Session::put('video-block', $blockId);
    }

}