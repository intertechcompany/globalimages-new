<?php namespace Intertech\Globalsite\Components;

use Redirect;
use Cms\Classes\ComponentBase;
use Intertech\Globalsite\Models\Page;
use Intertech\Globalsite\Models\HidePage;

class TextPage extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'TextPage Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'type' => 'string',
                'label' => 'slug'
            ]
        ];
    }

    public function onRun()
    {
        $slug = $this->property('slug');
        $text = Page::where('slug', $slug)->first();

        /**
         * Get Hide page for show page
         */
        $DL = HidePage::where('slug', $slug)->where('is_enabled', true)->first();

        if (isset($text) && count($text)) {
            $this->page->title = $text->main_title;
            $this->page['text'] = $text;
        } else {
            if (!empty($DL) && sizeof($DL) > 0) {
                $this->page->title = $DL->main_title;
                $this->page['text'] = $DL;
            } else {
                return Redirect::to('/404');
            }
        }
    }

}