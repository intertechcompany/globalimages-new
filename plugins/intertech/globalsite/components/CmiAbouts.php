<?php namespace Intertech\Globalsite\Components;

use Cms\Classes\ComponentBase;
use Intertech\Globalsite\Models\CmiAbout;

class CmiAbouts extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'CmiAbouts Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $articles = CmiAbout::where('is_enabled', true)
            ->orderBy('sort_order', 'asc')
            ->get();

        $this->page['articles'] = $articles;
    }

}