<?php namespace Intertech\Globalsite\Components;

use Cms\Classes\ComponentBase;
use Intertech\Globalsite\Models\Contact;

class Contacts extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Contacts Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $contacts = Contact::where('is_enabled', true)
                            ->orderBy('sort_order', 'asc')
                            ->get();

        $this->page['contacts'] = $contacts;
    }

}