<?php namespace Intertech\Globalsite\Components;

use Flash;
use Request;
use Session;
use Redirect;
use Validator;
use Cms\Classes\ComponentBase;
use System\Models\MailSetting;
use Illuminate\Support\Facades\Mail;
use Intertech\Globalsite\Models\Ask;
use Intertech\Globalsite\Models\Social;
use Intertech\Globalsite\Models\TypeQuery;
use Intertech\Globalsite\Models\ContactRequest;
use Intertech\Globalsite\Models\ContactSettings;
use Intertech\Globalsite\Traits\ComponentsTrait;

use Intertech\Globalsite\Models\Utm;
use Intertech\Globalsite\Models\CellBackSettings;

class CellBack extends ComponentBase
{
    use ComponentsTrait;

    public function componentDetails()
    {
        return [
            'name'        => 'CellBack Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'partial' => [
                'label' => 'Partial',
                'description' => 'Partial file name',
            ],
            'form' => [
                'label' => 'Partial',
                'description' => 'Partial file name',
            ],
        ];

    }

    public function onRun()
    {
        $request = Request::all();

        $this->page['utm'] = $this->getUTM($request);

        $this->page['utm_link'] = $this->getUtmByKey('utm_link');
        if (!$this->page['utm']) {
            $utm_id = $this->getUtmByKey('utm_id');
            $this->page['utm'] = Utm::find($utm_id);
        }
        $this->page['send_cellback'] = $this->getUtmByKey('send_cellback');
        // update session - send_cellback = false
        Session::put('send_cellback', false);
    }

    public function getUtmByKey($key)
    {
        return Session::get($key);
    }

    public function getUTM($request)
    {
        if (!empty($request)) {

            $getParm = http_build_query($request);
            
            $utm = Utm::where('utm', 'like', '%' . $getParm . '%')->first();

            if (!$utm) {
                return false;
            }

            return $utm;
        }
    }

    public function getData()
    {
        $form = $this->property('form');

        $socials = Social::where('is_enabled', true)
                         ->orderBy('sort_order', 'asc')
                         ->get();

        $ContactUs = ContactSettings::instance();

        if ($form == 'contact') {
            $types = ContactRequest::where('is_enabled', true)->orderBy('sort_order', 'asc')->get();
        }

        if (empty($form)) {
            $types = TypeQuery::where('is_enabled', true)->orderBy('sort_order', 'asc')->get();
        }
        
        return [
            'socials' => $socials,
            'types' => $types,
            'contact_us' => $ContactUs
        ];
    }

    public function onSendRequest()
    {
        $post = post();

        $rules = [
            // 'ask' => 'required',
            'name' => 'required|min:2',
            'email' => 'required|email',
            'phone' => 'required|min:6'
        ];

        $rules_message = [
            'message' => 'required|min:2|max:240'
        ];

        $validation = Validator::make($post, $rules);
        $validation_message = Validator::make($post, $rules_message);
        if (!$validation->fails()) {
            if (!$validation_message->fails()) {
                $model = new Ask;

                $model->fill(post());
                $model->ip_address = Request::getClientIp();

                $model->save();

                $utm_id = Request::get('utm_id');
                $utm = Utm::find($utm_id);

                if ($utm) {
                    $askModel = Ask::find($model->id);

                    $askModel->utm_title = $utm->title;
                    $askModel->utm_mark = $utm->utm;
                    $askModel->save();
                }

                /**
                 * Send email for user
                 */
                $this->onMailSent($utm);
                Session::put('send_cellback', true);

                // Flash::success('Ваш запрос успешно отправлен');
                return Redirect::to(Request::url());

                return [
                    'success' => true
                ];
            } else {
                // Flash::error('Ошибка отправки запроса');
                return [
                    'message' => true
                ];
            }
        } else {
            // Flash::error('Ошибка отправки запроса');
            return [
                'success' => false
            ];
        }
    }

    public function onMailSent($utm)
    {
        $data = [
            // 'ask' => post('ask'),
            'name' => post('name'),
            'email' => post('email'),
            'phone' => post('phone'),
            'body' => post('message'),
            'date' => date('d.m.Y'),
            'tops' => post('tops')
        ];

        $data['setting'] = CellBackSettings::instance();

        if ($utm) {
            $data['utm_title'] = $utm->title;
            $data['utm_mark'] = $utm->utm;
            $data['ip_address'] = Request::getClientIp();
        }

        Mail::send('intertech.globalsite::emails.feedback', $data, function($message) use ($data)
        {
            $message->to($data['email'], $data['name'])
                ->subject("Запит від {$data['name']}");
        });

        Mail::send('intertech.globalsite::emails.feedback_admin', $data, function($message) use ($data)
        {
            $message->to(MailSetting::get('sender_email'), $data['name'])
                ->subject("Запит від {$data['name']}");
        });

        // $user_form_name = MailSetting::get('sender_name');
        // $user_form_email = MailSetting::get('sender_email');
        // $user_to_name = $data['name'];
        // $subject = $data['ask'];


        // $headers = "From: $user_form_name <$user_form_email>\r\n". 
        //        "MIME-Version: 1.0" . "\r\n" . 
        //        "Content-type: text/html; charset=UTF-8" . "\r\n"; 

        // $message = $this->renderTemplate('feedback.htm', $data);

        // mail($data['email'], $subject, $message, $headers, '-f"' . $user_form_email . '" -F"' . $user_form_name . '"');

        // /**
        //  * Send for admin
        //  */

        // $admin_form_name = MailSetting::get('sender_name');
        // $admin_form_email = MailSetting::get('sender_email');
        // $admin_to_name = $data['name'];
        // $admin_to_email = $data['email'];

        // $headers_admin = "From: $admin_to_name <$admin_to_email>\r\n". 
        //        "MIME-Version: 1.0" . "\r\n" . 
        //        "Content-type: text/html; charset=UTF-8" . "\r\n"; 

        // mail($admin_form_email, $subject, $message, $headers_admin, '-f"' . $data['email'] . '" -F"' . $data['name'] . '"');
    }
}