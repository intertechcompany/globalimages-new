<?php namespace Intertech\Globalsite\Components;

use Cms\Classes\ComponentBase;
use Intertech\Globalsite\Models\Page;
use Intertech\Globalsite\Models\InformationSettings;

class Pages extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Pages Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->page['leftPages'] = Page::where('is_enabled', true)
            ->orderBy('sort_order', 'asc')
            ->where('is_main', true)
            ->where('position', 'left')
            ->get()
            ->toArray()
        ;
        $this->page['rightPages'] = Page::where('is_enabled', true)
            ->orderBy('sort_order', 'asc')
            ->where('is_main', true)
            ->where('position', 'right')
            ->get()
            ->toArray()
        ;
        $this->page['navigation_top_logo'] = InformationSettings::instance()->navigation_top_logo;

        $this->page['pages'] = Page::where('is_enabled', true)
            ->orderBy('sort_order', 'asc')
            ->where('is_main', true)
            ->get()
            ->toArray()
        ;
    }

}
