<?php namespace Intertech\Globalsite\Components;

use Session;
use Request;
use Cms\Classes\ComponentBase;
use Intertech\Globalsite\Models\Gettyimage;

class Gettyimages extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Videos Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        if (!empty(Request::get('hash'))) {
            Session::put('gettyimage-block', Request::get('hash'));
        }

        $gettys = Gettyimage::where('is_enabled', true)
                       ->orderBy('sort_order', 'asc')
                       ->get();

        $this->page['gettys'] = $gettys;

        $gettyId = Session::get('gettyimage-block');
        $gettySession = Gettyimage::find($gettyId);
        if ($gettySession) {
            $this->page['block'] = $gettySession;
        }
    }

    public function onShowForm()
    {
        $blockId = post('block_id');
        Session::put('gettyimage-block', $blockId);
    }

}