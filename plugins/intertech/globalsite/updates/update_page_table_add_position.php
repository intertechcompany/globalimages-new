<?php namespace Feegleweb\Octoshop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdatePageTableAddPosition extends Migration
{

    public function up() {
        Schema::table('intertech_globalsite_pages', function($table) {
            $table->string('position')->nullable();
        });
    }

    public function down() {
        Schema::table('intertech_globalsite_pages', function($table) {
            $table->dropColumn('position');
        });
    }

}
