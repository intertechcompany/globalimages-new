<?php namespace Feegleweb\Octoshop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdatePagesTable extends Migration
{

    public function up() {
        Schema::table('intertech_globalsite_pages', function($table) {
            $table->longText('sliders')->nullable();
        });
    }

    public function down() {
        Schema::table('intertech_globalsite_pages', function($table) {
            $table->dropColumn('sliders');
        });
    }

}
