<?php namespace Feegleweb\Octoshop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdatePageTableImage extends Migration
{

    public function up() {
        Schema::table('intertech_globalsite_pages', function($table) {
            $table->string('is_image')->nullable();
        });
    }

    public function down() {
        Schema::table('intertech_globalsite_pages', function($table) {
            $table->dropColumn('is_image');
        });
    }

}
