<?php namespace Feegleweb\Octoshop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdateCmiAboutTable extends Migration
{

    public function up() {
        Schema::table('intertech_globalsite_cmi_abouts', function($table) {
            $table->string('botton_tittle')->nullable();
        });
    }

    public function down() {
        Schema::table('intertech_globalsite_cmi_abouts', function($table) {
            $table->dropColumn('botton_tittle');
        });
    }

}
