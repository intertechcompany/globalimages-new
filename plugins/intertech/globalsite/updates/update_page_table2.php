<?php namespace Feegleweb\Octoshop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdatePageTable2 extends Migration
{

    public function up() {
        Schema::table('intertech_globalsite_pages', function($table) {
            $table->boolean('is_form_search')->default(true);
        });
    }

    public function down() {
        Schema::table('intertech_globalsite_pages', function($table) {
            $table->dropColumn('is_form_search');
        });
    }

}
