<?php namespace Feegleweb\Octoshop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdateHidePageTable extends Migration
{

    public function up() {
        Schema::table('intertech_globalsite_hide_pages', function($table) {
            $table->boolean('is_feedback')->default(true);
        });
    }

    public function down() {
        Schema::table('intertech_globalsite_hide_pages', function($table) {
            $table->dropColumn('is_feedback');
        });
    }

}
