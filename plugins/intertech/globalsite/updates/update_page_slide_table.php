<?php namespace Feegleweb\Octoshop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdatePageSlideTable extends Migration
{

    public function up() {
        Schema::table('intertech_globalsite_page_slide', function($table) {
            $table->timestamps();
        });
    }

    public function down() {
        Schema::table('intertech_globalsite_page_slide', function($table) {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }

}
