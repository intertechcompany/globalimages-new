<?php namespace Intertech\Globalsite\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePageSlidersTable extends Migration
{
    public function up()
    {
        Schema::create('intertech_globalsite_page_sliders', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('main_name')->nullable();
            $table->text('description')->nullable();
            $table->boolean('is_enabled')->default(true);
            $table->boolean('is_video')->default(false);
            $table->boolean('is_photo')->default(false);
            $table->text('link')->nullable();
            $table->integer('sort_order')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('intertech_globalsite_page_sliders');
    }
}
