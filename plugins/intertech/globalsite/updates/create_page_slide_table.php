<?php namespace Intertech\Globalsite\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePageSlideTable extends Migration
{
    public function up()
    {
        Schema::create('intertech_globalsite_page_slide', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('slide_id')->unsigned();
            $table->foreign('slide_id')
                ->references('id')
                ->on('intertech_globalsite_page_sliders')
                ->onDelete('cascade');
            $table->integer('page_id')->unsigned();
            $table->foreign('page_id')
                ->references('id')
                ->on('intertech_globalsite_pages')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('intertech_globalsite_page_slide');
    }
}
