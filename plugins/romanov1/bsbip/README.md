####Installation####
The plugin can be installed from the Marketplace. Please see the details [here](http://octobercms.com/help/site/projects#introduction).  

####Usage####
In  the 'System' section of the backend Settings page click to 'Backend secure by IP'  link. Go to the 'Manage IPs' section and add your IP to the list. After that return to the list and choose an action to perform for disallowed IPs.  Enable the protection when you finish configuring the IP addresses.

If you want deny access for IP from the list, choose option 'Disallow access to IP from the list'.  

> Try to don't disallow your current IP address! If this happens, you can use `artisan` command line tool to disable plugin or remove your IP from the list. See the details below.  

####Available options####

######Send HTTP status code######

To use this option you should create a page with url `/bsbip`. The plugin creates two Twig variables on this page:  

* `http_code` - with http status code what you choosen. 
* `bsbip` - which indicates that the used current plugin.  

Example:

```
url = "/bsbip"
==
function onStart()
{
    if(!$this['bsbip']){  
            $this->controller->setStatusCode(404);
            return \Response::make($this->controller->run('404'), 404);
    }
}
==
HTTP says: <h3>{{ http_code }}</h3>
```

######Redirect to CMS page######

This option allows you to redirect visitors from denied IP addresses  to one of your CMS pages.The list of pages is be available in the drop-down list.

######Show CMS page######

This option will show a selected page, but the request URL will remain the same. The list of pages is available in the drop-down list, like in the previous option.  

####Artisan commands####

* `bsbip:status` - show status of plugin (enabled or disabled)
* `bsbip:enable` - enable plugin
* `bsbip:disable` - disable plugin
* `bsbip:iplist` - show list of added IPs  

Options:

* `--add` - add IP address to the list
* `--remove` - remove IP address from the list

Example:
```
php artisan bsbip:iplist --add=127.0.0.1
```