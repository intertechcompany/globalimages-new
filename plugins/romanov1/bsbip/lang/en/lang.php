<?php
return [
    'plugin' => [
        'name' => 'Backend secure by IP',
        'description' => 'Control of access to the control panel by IP',
        'permissions' => 'Deny access to backend by IP',
    ],
    'settings' => [
        'enable' => 'Enable protect?',
        'enable_comment' => 'Check the IP address before access to the control panel',
        'current_ip' => 'Your current IP: ',
        'mode' => 'What to do with disallowed IPs?',
        'mode_status' => 'Send HTTP status code',
        'mode_redirect' => 'Redirect to page',
        'mode_content' => 'Show page',
        'target' => 'Select an available option',
        'tab_settings' => 'Settings',
        'tab_iplist' => 'IP list',
        'allow' => 'Allow access to IP from the list',
        'disallow' => 'Disallow access to IP from the list',
    ],
    'iplist' => [
        'title' => 'Manage IPs',
        'name' => 'IP address',
        'create' => 'Add IP',
        'comment' => 'Comment',
        'update' => 'Update IP',
        'delete' => 'Do you really want to delete this IP?',
        'delete_selected' => 'Do you really want to delete selected IPs?',
        'delete_selected_empty' => 'There are no selected IPs to delete.',
        'delete_selected_success' => 'Successfully deleted the selected IPs.',
    ],


];