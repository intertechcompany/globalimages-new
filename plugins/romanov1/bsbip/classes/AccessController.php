<?php namespace Romanov\Bsbip\Classes;

use Redirect;
use Romanov\Bsbip\Models\Settings as Settings;
use Romanov\Bsbip\Models\Ip as IpModel;

class AccessController
{
    private $ip;

    public function __construct($ip){
        $this->ip = $ip;
    }

    public function is_access_deny(){
        return $this->checkIp();
    }

    public function mode_run(){
        $mode = 'go_mode_'.Settings::get('mode');
        $target = Settings::get('target');
        return $this->$mode($target);
    }

    private function go_mode_status($target){
        $ct = new \Cms\Classes\Controller();
        $ct->vars['http_code'] = $target;
        $ct->vars['bsbip'] = true;
        $ct->setStatusCode((int)$target);
        return $ct->run('/bsbip');
    }

    private function go_mode_redirect($target){
        return Redirect::to($target);
    }

    private function go_mode_content($target){
        $ct = new \Cms\Classes\Controller();
        return $ct->run($target);
    }

    private function checkIp(){
        $ip_mode = Settings::get('ip_mode');
        switch($ip_mode){
            case 'allow':   return $this->ci_allow();
            case 'disallow':    return $this->ci_disallow();
            default:    return false;
        }
    }

    private function ci_allow(){
        return (IpModel::where('ip', '=', $this->ip)->first()) ? false : true;
    }

    private function ci_disallow(){
        return (IpModel::where('ip', '=', $this->ip)->first()) ? true : false;
    }
}