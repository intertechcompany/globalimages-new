<?php namespace Romanov\Bsbip\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateIpsTable extends Migration
{

    public function up()
    {
        Schema::create('romanov_bsbip_ips', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('ip', 45)->unique();
            $table->string('comment')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('romanov_bsbip_ips');
    }

}
