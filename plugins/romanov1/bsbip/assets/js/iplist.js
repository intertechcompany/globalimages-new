+function ($) { "use strict";

    var IpList = function() {

        this.clickRecord = function(recordId) {
            var newPopup = $('<a />')

            newPopup.popup({
                handler: 'onUpdateForm',
                extraData: {
                    'record_id': recordId,
                }
            })
        }

        this.createRecord = function() {
            var newPopup = $('<a />')
            newPopup.popup({ handler: 'onCreateForm' })
        }

    }

    $.ipList = new IpList;

}(window.jQuery);