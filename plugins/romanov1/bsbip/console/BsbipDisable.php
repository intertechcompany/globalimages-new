<?php
namespace Romanov\Bsbip\Console;

use Illuminate\Console\Command;
use Romanov\Bsbip\Models\Settings as Settings;

class BsbipDisable extends Command
{
    protected $name = 'bsbip:disable';
    protected $description = 'Disable backend secure by ip';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        if(Settings::set('is_enabled', false)){
            $this->info('The command completed successfully');
            $this->output->write('bsbib current status: ');
            $this->comment('disabled');
        }else{
            $this->error('Error: Something went wrong!');
        }
    }
}