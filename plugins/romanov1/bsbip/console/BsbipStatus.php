<?php
namespace Romanov\Bsbip\Console;

use Illuminate\Console\Command;
use Romanov\Bsbip\Models\Settings as Settings;

class BsbipStatus extends Command
{
    protected $name = 'bsbip:status';
    protected $description = 'Backend secure by ip status';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $bsbip = Settings::get('is_enabled', false);
        $this->output->write('bsbib current status: ');
        if($bsbip){
            $this->comment('enabled');
        }else{
            $this->comment('disabled');
        }
    }
}