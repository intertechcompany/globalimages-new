<?php
namespace Romanov\Bsbip\Console;

use Illuminate\Console\Command;
use Romanov\Bsbip\Models\Settings as Settings;

class BsbipEnable extends Command
{
    protected $name = 'bsbip:enable';
    protected $description = 'Enable backend secure by ip';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        if(Settings::set('is_enabled', true)){
            $this->info('The command completed successfully');
            $this->output->write('bsbib current status: ');
            $this->comment('enabled');
        }else{
            $this->error('Error: Something went wrong!');
        }
    }
}