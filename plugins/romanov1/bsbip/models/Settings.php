<?php namespace Romanov\Bsbip\Models;

use Model;
use Lang;
use Cms\Classes\Page;

class Settings extends Model
{
    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'mode' => 'required',
        'target' => 'required',
        'ip_mode' => 'required',
    ];
    public $customMessages;
    public $implement = ['System.Behaviors.SettingsModel'];
    public $settingsCode = 'romanov_bsbip_settings';
    public $settingsFields = 'fields.yaml';

    public function __construct()
    {
        $this->customMessages = [
            'mode.required' => Lang::get('romanov.cel::lang.settings.mode'),
            'target.required' => Lang::get('romanov.cel::lang.settings.target'),

        ];
        parent::__construct();
    }

    public function getModeOptions()
    {
        return [
            'status' => Lang::get('romanov.bsbip::lang.settings.mode_status'),
            'redirect' => Lang::get('romanov.bsbip::lang.settings.mode_redirect'),
            'content' => Lang::get('romanov.bsbip::lang.settings.mode_content')
        ];
    }

    public function getTargetOptions(){
        if ($this->mode == 'redirect' || $this->mode == 'content') {
            return Page::sortBy('baseFileName')->lists('baseFileName', 'url');
        }else{
            return [
                '400' => '400 Bad Request',
                '401' => '401 Unauthorized',
                '403' => '403 Forbidden',
                '404' => '404 Not Found',
                '418' => '418 I\'m a teapot',
                '423' => '423 Locked'
            ];
        }
    }

}

