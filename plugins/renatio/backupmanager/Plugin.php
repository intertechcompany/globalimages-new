<?php namespace Renatio\BackupManager;

use Backend;
use Illuminate\Support\Facades\Artisan;
use Renatio\BackupManager\Models\Settings;
use System\Classes\PluginBase;
use App;

/**
 * BackupManager Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'renatio.backupmanager::lang.plugin.name',
            'description' => 'renatio.backupmanager::lang.plugin.description',
            'author'      => 'Renatio',
            'icon'        => 'icon-database'
        ];
    }

    /**
     * Register backend navigation.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'backupmanager' => [
                'label'       => 'renatio.backupmanager::lang.navigation.backupmanager',
                'url'         => Backend::url('renatio/backupmanager/backups'),
                'icon'        => 'icon-database',
                'permissions' => ['renatio.backupmanager.access_backups'],
                'order'       => 500,
                'sideMenu'    => [
                    'backups' => [
                        'label'       => 'renatio.backupmanager::lang.navigation.backups',
                        'icon'        => 'icon-archive',
                        'url'         => Backend::url('renatio/backupmanager/backups'),
                        'permissions' => ['renatio.backupmanager.access_backups']
                    ]
                ]
            ]
        ];
    }

    /**
     * Register permissions.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'renatio.backupmanager.access_settings' => [
                'label' => 'renatio.backupmanager::lang.permissions.access_settings_label',
                'tab'   => 'renatio.backupmanager::lang.permissions.tab'
            ],
            'renatio.backupmanager.access_backups'  => [
                'label' => 'renatio.backupmanager::lang.permissions.access_backups_label',
                'tab'   => 'renatio.backupmanager::lang.permissions.tab'
            ]
        ];
    }

    /**
     * Register settings.
     *
     * @return array
     */
    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'renatio.backupmanager::lang.settings.label',
                'description' => 'renatio.backupmanager::lang.settings.description',
                'category'    => 'renatio.backupmanager::lang.settings.category',
                'icon'        => 'icon-database',
                'class'       => 'Renatio\BackupManager\Models\Settings',
                'order'       => 500,
                'keywords'    => 'backup manager database',
                'permissions' => ['renatio.backupmanager.access_settings']
            ]
        ];
    }

    /**
     * Register console commands
     */
    public function register()
    {
        $this->registerConsoleCommand('backup.clean', 'Renatio\BackupManager\Console\Clean');
        $this->registerConsoleCommand('backup.run', 'Renatio\BackupManager\Console\Backup');
        $this->registerConsoleCommand('backup.restore', 'Renatio\BackupManager\Console\Restore');
    }

    /**
     * Register scheduled commands
     *
     * @param $schedule
     */
    public function registerSchedule($schedule)
    {
        $settings = Settings::instance();

        // Backup database
        if ($settings['db_scheduler']) {
            $schedule->call(function () {
                Artisan::call('backup:run', ['--only-db' => true]);

            })->{$settings['db_scheduler']}();
        }

        // Backup application
        if ($settings['app_scheduler']) {
            $schedule->command('backup:run')->{$settings['app_scheduler']}();
        }

        // Clean old backups
        if ($settings['clean_scheduler']) {
            $schedule->command('backup:clean')->{$settings['clean_scheduler']}();
        }
    }

    /**
     * Boot service provider
     */
    public function boot()
    {
        App::register('Renatio\BackupManager\DropboxFilesystemServiceProvider');
    }
}