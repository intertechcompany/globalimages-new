<?php namespace Renatio\BackupManager\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

/**
 * Class CreateBackupsTable
 * @package Renatio\BackupManager\Updates
 */
class CreateBackupsTable extends Migration
{

    /**
     * Migrate up
     */
    public function up()
    {
        Schema::create('renatio_backupmanager_backups', function ($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('disk_name');
            $table->string('file_path');
            $table->enum('type', ['db', 'app']);
            $table->string('filesystems');
            $table->integer('file_size');
            $table->timestamps();
        });
    }

    /**
     * Migrate down
     */
    public function down()
    {
        Schema::dropIfExists('renatio_backupmanager_backups');
    }

}