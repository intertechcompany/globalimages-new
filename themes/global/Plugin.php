<?php namespace Intertech\Globalsite;

use Backend;
use System\Classes\PluginBase;
use Cms\Classes\CmsController;
use Intertech\Globalsite\Models\slider as HomeSlider;

/**
 * Globalsite Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Globalsite',
            'description' => 'No description provided yet...',
            'author'      => 'Intertech',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        CmsController::extend(function($controller) {
            $controller->middleware('Intertech\Globalsite\Middlewares\DetermineLocation');
        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {

        return [
            'Intertech\Globalsite\Components\Pages' => 'navigation',
            'Intertech\Globalsite\Components\Sliders' => 'sliders',
            'Intertech\Globalsite\Components\Abouts' => 'home_abouts',
            'Intertech\Globalsite\Components\Galeries' => 'home_galeries',
            'Intertech\Globalsite\Components\Advantage' => 'home_advantages',
            'Intertech\Globalsite\Components\Posts' => 'home_posts',
            'Intertech\Globalsite\Components\MainBg' => 'main_bg',
            'Intertech\Globalsite\Components\Photos' => 'photos',
            'Intertech\Globalsite\Components\FooterOrder' => 'footer_order',
            'Intertech\Globalsite\Components\Musics' => 'musics',
            'Intertech\Globalsite\Components\Subscriptions' => 'subscriptions',
            'Intertech\Globalsite\Components\Contacts' => 'contacts',
            'Intertech\Globalsite\Components\Map' => 'map',
            'Intertech\Globalsite\Components\ContactDescription' => 'contact_description',
            'Intertech\Globalsite\Components\Footer' => 'footer',
            'Intertech\Globalsite\Components\CellBack' => 'cell_back',
            'Intertech\Globalsite\Components\Services' => 'services',
            'Intertech\Globalsite\Components\TextPage' => 'text_page',
            'Intertech\Globalsite\Components\Videos' => 'video',
            'Intertech\Globalsite\Components\ErrorPage' => 'error_page',
            'Intertech\Globalsite\Components\DedaultLeyaut' => 'default_leyaut',
            'Intertech\Globalsite\Components\CmiAbouts' => 'abouts',
            'Intertech\Globalsite\Components\Search' => 'search',
            'Intertech\Globalsite\Components\Gettyimages' => 'gettyimage',
            'Intertech\Globalsite\Components\FormedPath' => 'formedpath',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'intertech.globalsite.access_posts' => [
                'label' => 'Управление основной инфомацией',
                'tab' => 'Основная информация'
            ]
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {

        return [
            'globalsite' => [
                'label'       => 'Основная информация',
                'url'         => Backend::url('intertech/globalsite/pages'),
                'icon'        => 'icon-leaf',
                'permissions' => ['intertech.globalsite.*'],
                'order'       => 500,
                'sideMenu' => [
                    'pages' => [
                        'label'       => 'Страницы',
                        'icon'        => 'icon-file-text',
                        'url'         => Backend::url('intertech/globalsite/pages'),
                        'permissions' => ['intertech.globalsite.*']
                    ],
                    'hidepages' => [
                        'label'       => 'Скрытые страницы',
                        'icon'        => 'icon-file-text',
                        'url'         => Backend::url('intertech/globalsite/hidepages'),
                        'permissions' => ['intertech.globalsite.*']
                    ],
                    'sliders' => [
                        'label'       => 'Слайдер',
                        'icon'        => 'icon-indent',
                        'url'         => Backend::url('intertech/globalsite/sliders'),
                        'permissions' => ['intertech.globalsite.*']
                    ],
                    'photos' => [
                        'label'       => 'Фото',
                        'icon'        => 'icon-file-image-o',
                        'url'         => Backend::url('intertech/globalsite/photos'),
                        'permissions' => ['intertech.globalsite.*']
                    ],
                    'videos' => [
                        'label'       => 'Видео',
                        'icon'        => 'icon-file-video-o',
                        'url'         => Backend::url('intertech/globalsite/videos'),
                        'permissions' => ['intertech.globalsite.*']
                    ],
                    'musics' => [
                        'label'       => 'Музыка',
                        'icon'        => 'icon-music',
                        'url'         => Backend::url('intertech/globalsite/musics'),
                        'permissions' => ['intertech.globalsite.*']
                    ],
                    // 'gettyimages' => [
                    //     'label'       => 'GettyImages',
                    //     'icon'        => 'icon-music',
                    //     'url'         => Backend::url('intertech/globalsite/gettyimages'),
                    //     'permissions' => ['intertech.globalsite.*']
                    // ],
                    'services' => [
                        'label'       => 'Сервисы',
                        'icon'        => 'icon-server',
                        'url'         => Backend::url('intertech/globalsite/services'),
                        'permissions' => ['intertech.globalsite.*']
                    ],
                    'contacts' => [
                        'label'       => 'Контакты',
                        'icon'        => 'icon-phone',
                        'url'         => Backend::url('intertech/globalsite/contacts'),
                        'permissions' => ['intertech.globalsite.*']
                    ],
                    'galeries' => [
                        'label'       => 'Галерея',
                        'icon'        => 'icon-list-alt',
                        'url'         => Backend::url('intertech/globalsite/galeries'),
                        'permissions' => ['intertech.globalsite.*']
                    ],
                    'posts' => [
                        'label'       => 'Посты',
                        'icon'        => 'icon-th-large',
                        'url'         => Backend::url('intertech/globalsite/posts'),
                        'permissions' => ['intertech.globalsite.*']
                    ],
                    'socials' => [
                        'label'       => 'Соц. сети',
                        'icon'        => 'icon-vk',
                        'url'         => Backend::url('intertech/globalsite/socials'),
                        'permissions' => ['intertech.globalsite.*']
                    ],
                    'subscriptions' => [
                        'label'       => 'Подписка',
                        'icon'        => 'icon-list-ol',
                        'url'         => Backend::url('intertech/globalsite/subscriptions'),
                        'permissions' => ['intertech.globalsite.*']
                    ],
                    'cmiabouts' => [
                        'label'       => 'СМИ о нас',
                        'icon'        => 'icon-credit-card',
                        'url'         => Backend::url('intertech/globalsite/CmiAbouts'),
                        'permissions' => ['intertech.globalsite.*']
                    ],
                ],
            ],
            'forms' => [
                'label'       => 'Формы',
                'url'         => Backend::url('intertech/globalsite/asks'),
                'icon'        => 'icon-envelope-o',
                'permissions' => ['intertech.globalsite.*'],
                'order'       => 600,
                'sideMenu' => [
                    'asks' => [
                        'label'       => 'Вопросы',
                        'icon'        => 'icon-envelope-o',
                        'url'         => Backend::url('intertech/globalsite/asks'),
                        'permissions' => ['intertech.globalsite.*']
                    ],
                    'typequeris' => [
                        'label'       => 'Тип запроса',
                        'icon'        => 'icon-file-text-o',
                        'url'         => Backend::url('intertech/globalsite/typequeris'),
                        'permissions' => ['intertech.globalsite.*']
                    ],
                    'contactrequests' => [
                        'label'       => 'Тип запроса для контактов',
                        'icon'        => 'icon-external-link',
                        'url'         => Backend::url('intertech/globalsite/contactrequests'),
                        'permissions' => ['intertech.globalsite.*']
                    ],
                    'utms' => [
                        'label'       => 'Формы',
                        'icon'        => 'icon-bookmark',
                        'url'         => Backend::url('intertech/globalsite/utms'),
                        'permissions' => ['intertech.globalsite.*']
                    ],
                ],
            ],
            'home' => [
                'label'       => 'Информация на главной',
                'url'         => Backend::url('intertech/globalsite/abouts'),
                'icon'        => 'icon-home',
                'permissions' => ['intertech.globalsite.*'],
                'order'       => 700,
                'sideMenu' => [
                    'abouts' => [
                        'label'       => 'О нас',
                        'icon'        => 'icon-home',
                        'url'         => Backend::url('intertech/globalsite/abouts'),
                        'permissions' => ['intertech.globalsite.*']
                    ],
                    'advantages' => [
                        'label'       => 'Преимущества',
                        'icon'        => 'icon-copy',
                        'url'         => Backend::url('intertech/globalsite/advantages'),
                        'permissions' => ['intertech.globalsite.*']
                    ],
                ],
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'contacts' => [
                'label'       => 'Контакты',
                'description' => 'Редактирование страницы "Контактов"',
                'category'    => 'Сайт',
                'icon'        => 'icon-phone',
                'class'       => 'intertech\globalsite\Models\ContactSettings',
                'order'       => 100,
            ],
            // 'contactus' => [
            //     'label'       => 'Связаться с нами',
            //     'description' => 'Редактирование формы "Связаться с нами"',
            //     'category'    => 'Сайт',
            //     'icon'        => 'icon-phone',
            //     'class'       => 'intertech\globalsite\Models\ContactUsSettings',
            //     'order'       => 101,
            // ],
            'information' => [
                'label'       => 'Информация о сайте',
                'description' => 'Редактирование Футера, Подписок',
                'category'    => 'Сайт',
                'icon'        => 'icon-info-circle',
                'class'       => 'intertech\globalsite\Models\InformationSettings',
                'order'       => 102,
            ],
            'errorpage' => [
                'label'       => 'Страница 404',
                'description' => 'Редактирование страницы 404',
                'category'    => 'Сайт',
                'icon'        => 'icon-info-circle',
                'class'       => 'intertech\globalsite\Models\ErrorSettings',
                'order'       => 102,
            ],
            'cellback' => [
                'label'       => 'Связаться с нами',
                'description' => 'Редактирование формы отправки "Связаться с нами"',
                'category'    => 'Сайт',
                'icon'        => 'icon-info-circle',
                'class'       => 'intertech\globalsite\Models\CellBackSettings',
                'order'       => 103,
            ],
        ];
    }

    public function registerMarkupTags()
    {
        return [
            'functions' => [
                'getImage' => [$this, 'getImageForSlide']
            ]
        ];
    }

    public function getImageForSlide($string, $image = 'center_image')
    {
        return $slider = HomeSlider::where('id', $string)->first()->$image;
    }

}
